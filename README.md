# TEMPER #

### What is this repository for? ###

this repository is for Temper hiring tech assessment [Read more](https://docs.google.com/document/d/14EhAtRp3XVCkIcl68WXPRPlD6pzx0_LDMDBv-GnP70E/edit)

### How to run? ###

* deploy the code to a server (or local env) which runs php
* open the project URL in browser

### Screen shot ###

![Screenshot](Screenshot.jpg)

### How to run tests? ###

* open terminal
* go to project root
* run `./vendor/bin/phpunit --bootstrap vendor/autoload.php -v tests`