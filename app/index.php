<?php
/**
 * index.php
 *
 * @author: Ali Jafari <ali.jafari.pw@gmail.com>
 * @date: 14/11/19
 */
require 'vendor/autoload.php';

use App\Data\DataFactory;

const APP_DIR = __DIR__;
const DATA_FACTORY = 'Csv';

if (!isset($_GET['dataType'])) {
    echo 'Hello World!';
    exit(0);
}

header('Content-Type: application/json');


switch ($_GET['dataType']) {
    case 'weekly_retention':

        $customerFunnelData = DataFactory::getData()->getWeeklyChartData();

        echo json_encode($customerFunnelData);
        exit(0);
    default:
        echo json_encode(['error' => 'Invalid dataType!']);
        http_response_code(400);
        exit(0);
}