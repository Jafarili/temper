<?php
/**
 * Csv
 *
 * @author: Ali Jafari <ali.jafari.pw@gmail.com>
 * @date: 14/11/19
 */
namespace App\Data;


class Csv extends AbstractData
{
    public function getCustomersFunnelData()
    {
        $filePath = APP_DIR . '/../data/export.csv';
        $handle = fopen($filePath, 'rb');
        if (!$handle) {
            throw new \RuntimeException('Could not read export.csv file');
        }

        // Skip first line of CSV file
        fgets($handle);

        $records = [];
        while (($line = fgets($handle)) !== false) {
            $record = explode(';', trim($line));
            $records[] = [
                'user_id' => $record[0],
                'created_at' => $record[1],
                'onboarding_perentage' => $record[2],
                'count_applications' => $record[3],
                'count_accepted_applications' => $record[4],
            ];
        }

        return $records;
    }
}