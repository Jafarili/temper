<?php
/**
 * DataFactory.php
 *
 * @author: Ali Jafari <ali.jafari.pw@gmail.com>
 * @date: 14/11/19
 */
namespace App\Data;

class DataFactory
{
    public static function getData(): AbstractData
    {
        switch (DATA_FACTORY) {
            case 'Csv':
                return new Csv();
            default:
                throw new \RuntimeException('Invalid Data Factory config');
        }
    }
}