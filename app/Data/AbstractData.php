<?php
/**
 * AbstractData.php
 *
 * @author: Ali Jafari <ali.jafari@wundermobility.com>
 * @date: 14/11/19
 */

namespace App\Data;


abstract class AbstractData
{
    abstract public function getCustomersFunnelData();

    public function getCustomersWeekly()
    {
        $customerFunnelData = $this->getCustomersFunnelData();

        // Making sure that the data is sorted by created_at
        usort($customerFunnelData, function($a, $b) {
            return strtotime($a['created_at']) - strtotime($b['created_at']);
        });
        $firstRecordTime = new \DateTime($customerFunnelData[0]['created_at']);

        $customersByWeek = [];
        foreach ($customerFunnelData as $customerFunnelDatum) {
            $days = (new \DateTime($customerFunnelDatum['created_at']))
                ->diff($firstRecordTime)
                ->days;

            $weekNumber = (int)floor($days / 7);
            if (!isset($customersByWeek[$weekNumber])) {
                $customersByWeek[$weekNumber] = [];
            }
            $customersByWeek[$weekNumber][] = $customerFunnelDatum;
        }

        return $customersByWeek;
    }

    public function getWeeklyChartData(): array
    {
        $levels = [
            0 => 'Create account',
            20 => 'Activate account',
            40 => 'Provide profile information',
            50 => 'What jobs are you interested in?',
            70 => 'Do you have relevant experience in these jobs?',
            90 => 'Are you a freelancer?',
            99 => 'Waiting for approval',
            100 => 'Approval',
        ];

        $customersByWeek = $this->getCustomersWeekly();

        $return = [];
        foreach ($customersByWeek as $weekNumber => $customers) {
            $customersCount = count($customers);
            $customerWeekLevelCount = array_fill_keys(array_keys($levels), 0);
            $levelPercentage = [];
            foreach ($levels as $levelID => $levelName) {
                foreach ($customers as $customer) {
                    if ($customer['onboarding_perentage'] >= $levelID) {
                        $customerWeekLevelCount[$levelID]++;
                        $levelPercentage[$levelID] = round(($customerWeekLevelCount[$levelID] / $customersCount) * 100, 2);
                    }
                }
            }

            $return[] = [
                'name' => "Week $weekNumber",
                'data' => array_values($levelPercentage),
            ];
        }


        return $return;
    }
}
