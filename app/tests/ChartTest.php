<?php
/**
 * Chart Test
 *
 * @author: Ali Jafari <ali.jafari.pw@gmail.com>
 * @date: 15/11/19
 */
declare(strict_types=1);

class ChartTest extends \PHPUnit\Framework\TestCase
{
    /** @var \App\Data\AbstractData */
    public $data;

    public $weeklyChartData;

    protected function setUp()
    {
        $this->data = $this->getMockBuilder(\App\Data\Csv::class)
            ->setMethods(['getCustomersFunnelData'])
            ->getMock();

        $this->data->method('getCustomersFunnelData')->willReturn([
            [
                'user_id' => '3121',
                'created_at' => '2016-07-19',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3124',
                'created_at' => '2016-07-19',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3126',
                'created_at' => '2016-07-20',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3131',
                'created_at' => '2016-07-20',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3132',
                'created_at' => '2016-07-20',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3134',
                'created_at' => '2016-07-21',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3135',
                'created_at' => '2016-07-21',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3136',
                'created_at' => '2016-07-21',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3138',
                'created_at' => '2016-07-21',
                'onboarding_perentage' => '50',
            ],
            [
                'user_id' => '3150',
                'created_at' => '2016-07-22',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3166',
                'created_at' => '2016-07-22',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3167',
                'created_at' => '2016-07-23',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3169',
                'created_at' => '2016-07-23',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3173',
                'created_at' => '2016-07-23',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3175',
                'created_at' => '2016-07-23',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3187',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3188',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3189',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3190',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '50',
            ],

            [
                'user_id' => '3204',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3205',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3212',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3213',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3214',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3215',
                'created_at' => '2016-07-24',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3256',
                'created_at' => '2016-07-25',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3257',
                'created_at' => '2016-07-25',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3258',
                'created_at' => '2016-07-25',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3259',
                'created_at' => '2016-07-25',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3260',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '95',
            ],
            [
                'user_id' => '3261',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3262',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3263',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3264',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3265',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3296',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3298',
                'created_at' => '2016-07-26',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3311',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '50',
            ],
            [
                'user_id' => '3312',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3313',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '95',
            ],
            [
                'user_id' => '3314',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3315',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3321',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3322',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '60',
            ],
            [
                'user_id' => '3323',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3330',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '95',
            ],
            [
                'user_id' => '3331',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '',
            ],
            [
                'user_id' => '3332',
                'created_at' => '2016-07-27',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3339',
                'created_at' => '2016-07-28',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3340',
                'created_at' => '2016-07-28',
                'onboarding_perentage' => '55',
            ],
            [
                'user_id' => '3341',
                'created_at' => '2016-07-28',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3342',
                'created_at' => '2016-07-28',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3343',
                'created_at' => '2016-07-28',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3377',
                'created_at' => '2016-07-30',
                'onboarding_perentage' => '60',
            ],
            [
                'user_id' => '3378',
                'created_at' => '2016-07-30',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3379',
                'created_at' => '2016-07-30',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3380',
                'created_at' => '2016-07-30',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3382',
                'created_at' => '2016-07-30',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3383',
                'created_at' => '2016-07-30',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3384',
                'created_at' => '2016-07-30',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3392',
                'created_at' => '2016-07-31',
                'onboarding_perentage' => '95',
            ],
            [
                'user_id' => '3394',
                'created_at' => '2016-07-31',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3395',
                'created_at' => '2016-07-31',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3406',
                'created_at' => '2016-08-01',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3407',
                'created_at' => '2016-08-01',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3408',
                'created_at' => '2016-08-01',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3409',
                'created_at' => '2016-08-01',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3410',
                'created_at' => '2016-08-01',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3411',
                'created_at' => '2016-08-01',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3413',
                'created_at' => '2016-08-02',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3434',
                'created_at' => '2016-08-03',
                'onboarding_perentage' => '50',
            ],
            [
                'user_id' => '3435',
                'created_at' => '2016-08-03',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3438',
                'created_at' => '2016-08-03',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3439',
                'created_at' => '2016-08-03',
                'onboarding_perentage' => '65',
            ],
            [
                'user_id' => '3440',
                'created_at' => '2016-08-03',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3441',
                'created_at' => '2016-08-03',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3442',
                'created_at' => '2016-08-03',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3454',
                'created_at' => '2016-08-04',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3455',
                'created_at' => '2016-08-04',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3456',
                'created_at' => '2016-08-04',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3457',
                'created_at' => '2016-08-04',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3458',
                'created_at' => '2016-08-04',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3459',
                'created_at' => '2016-08-05',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3469',
                'created_at' => '2016-08-06',
                'onboarding_perentage' => '65',
            ],
            [
                'user_id' => '3474',
                'created_at' => '2016-08-07',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3475',
                'created_at' => '2016-08-07',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3478',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3479',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3480',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3481',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3482',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3483',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3484',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3485',
                'created_at' => '2016-08-08',
                'onboarding_perentage' => '100',
            ],
            [
                'user_id' => '3494',
                'created_at' => '2016-08-09',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3508',
                'created_at' => '2016-08-09',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3509',
                'created_at' => '2016-08-09',
                'onboarding_perentage' => '99',
            ],
            [
                'user_id' => '3510',
                'created_at' => '2016-08-09',
                'onboarding_perentage' => '40',
            ],
            [
                'user_id' => '3512',
                'created_at' => '2016-08-09',
                'onboarding_perentage' => '50',
            ],
            [
                'user_id' => '3526',
                'created_at' => '2016-08-10',
                'onboarding_perentage' => '20',
            ],
        ]);
        $this->weeklyChartData = $this->data->getWeeklyChartData();
    }

    // asserts week count and week names
    public function testWeeks()
    {
        $this->assertCount(4, $this->weeklyChartData);
        $this->assertEquals('Week 0', $this->weeklyChartData[0]['name']);
        $this->assertEquals('Week 1', $this->weeklyChartData[1]['name']);
        $this->assertEquals('Week 2', $this->weeklyChartData[2]['name']);
        $this->assertEquals('Week 3', $this->weeklyChartData[3]['name']);
    }

    // asserts numbers of each chart are decreasing
    public function testDecreasingFlow()
    {
        foreach ($this->weeklyChartData as $weekData) {
            $temp = 100;
            foreach ($weekData['data'] as $value) {
                $this->assertLessThanOrEqual($temp, $value);
                $temp = $value;
            }
        }
    }

    // asserts first number of each week is 100
    public function testFirstNumberOfEachWeek()
    {
        foreach ($this->weeklyChartData as $weekData) {
            $this->assertEquals(100, $weekData['data'][0]);
        }
    }
}
